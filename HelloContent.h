#ifndef HELLOCONTENT_H
#define HELLOCONTENT_H

#include "Content.h"
#include "EventTable.h"
#include <vector>
#include "MorseContent.h"

using namespace std;

class HelloContent: public Content
{
protected:
	vector<Object*> objects;
	MorseContent* morseContent;
public:
	HelloContent(MainWindow* mw, Content* parent, EventTable& et): Content(mw, parent, et)
	{
		objects.push_back( new Object(10, 10, 700, 90, "Wezwij pomoc") );
		objects.push_back( new Object(10, 110, 700, 90, "Nadaj alfabetem morse'a") );
		objects.push_back( new Object(10, 210, 700, 90, "Nadaj za pomocą T9") );
		morseContent = new MorseContent(mw, this, et);
	}
	virtual void init();
	virtual void routeSelection(int sel);
	static void* childProcess(void *);
};


#endif
