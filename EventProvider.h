#ifndef EVENTPROVIDER_H
#define EVENTPROVIDER_H

#include "EventTable.h"
#include <pthread.h>
#include "Event.h"

class EventProvider
{
protected:
	EventTable& eventTable;
public:
	EventProvider(EventTable& et): eventTable(et)
	{ }

	virtual bool start() = 0;
	virtual void shutdown() = 0;
};

#endif
