#ifndef MORSECONTENT_H
#define MORSECONTENT_H

#include "Content.h"

class MorseContent: public Content
{
protected:
	pthread_t thread;
public:
	MorseContent(MainWindow* mw, Content* p, EventTable& et): Content(mw, p, et)
	{ }

	virtual void init()
	{
		mainWindow->clear();
		std::cout << "Morse init() " << std::endl;
		pthread_create(&thread, NULL, procedure, (void*)this);
		
	}

	static void* procedure(void* data)
	{
		MorseContent* self = (MorseContent*)data;
		bool goBack = false;
		while(true)
		{
			vector<Event*>* events = self->eventTable.getEvents();
			if(events->size() > 0) goBack = true;
			self->eventTable.clear();
			self->eventTable.release();
			if(goBack)
			{
				std::cout << "MorseContent wants to terminate itself" << std::endl;
				self->wantToGoHome();
				return nullptr;	
			}
			usleep(500 * 1000);
		}
	}
};

#endif
