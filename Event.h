#ifndef EVENT_H
#define EVENT_H

#include <sstream>
enum EventType
{
	EYE_CLOSED = 0, 
	EYE_OPENED = 1
};


struct Event
{
	Event(EventType et, int t): eventType(et), time(t)
	{}
	EventType eventType;
	int time;

	std::string toString() const
	{
		std::stringstream s;
		s << "EventType: " << eventType << " time: " << time;
		return s.str();
	}
	
};

#endif
