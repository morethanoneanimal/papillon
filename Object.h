#ifndef OBJECT_H
#define OBJECT_H
#include <gtk/gtk.h>
#include <cairo.h>
#include <string>
#include "Utils.h"

class Object
{
protected:
	std::string text;
	int state;
	int x, y, width, height;
public:
	static const int NOTHING = 0;
	static const int SELECTED = 1;
	static const int CLICKED = 2;
	static const int CONFIRMED = 3;
	Object(int _x, int _y, int _width, int _height, std::string t = ""): text(t), state(NOTHING), x(_x), y(_y), width(_width), height(_height)
	{ }
	void draw(cairo_t* ct)
	{
		if(state == NOTHING) Utils::setNeutralColor(ct);
		else if(state == SELECTED) Utils::setSelectedColor(ct);
		else if(state == CLICKED) Utils::setClickedColor(ct);
		else if(state == CONFIRMED) Utils::setConfirmedColor(ct);
		cairo_rectangle(ct, x, y, width, height);
		cairo_stroke_preserve(ct);
		cairo_fill(ct);			
		cairo_set_source_rgb(ct, 1, 1, 1);
		cairo_move_to(ct, x + 10, y + 40);
		cairo_show_text(ct, text.c_str());
	}

	int getState() const
	{ return state; }
	void setState(int newState) 
	{
		state = newState;
	}
	void select()
	{ setState(SELECTED); }
	void deselect()
	{ setState(NOTHING); }
	void clicked() 
	{ setState(CLICKED); }
	void confirmed()
	{ setState(CONFIRMED); }

	
};

#endif
