#ifndef CONTENT_H
#define CONTENT_H

#include <vector>
#include "Object.h"
#include "MainWindow.h"
#include "EventTable.h"
class Content
{
protected:
	std::vector<Content*> subContents;
	int selection;
	MainWindow* mainWindow;
	Content* parent;
	EventTable& eventTable;
public:
	Content(MainWindow* win, Content* p, EventTable& et): mainWindow(win), parent(p), eventTable(et)
	{ }
	virtual void init() = 0;
	virtual void kidWantsToGoHome()
	{
		selection = -1; 
		init();
	}

	virtual void wantToGoHome()
	{
		if(parent)
			parent->kidWantsToGoHome();
	}
	
};


#endif 
