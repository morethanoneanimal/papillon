#ifndef WEBCAM_OPERATOR_H
#define WEBCAM_OPERATOR_H
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <sstream>
#include <vector>
#include "Calibration.h"
#include "Recorder.h"


using namespace cv;
using namespace std;

class WebcamOperator
{
protected:
	Mat frame, part, roleModel, originalFrame;
	bool eyeState;
//	HorizontalImgAnalyzer horizontalAnalyzer;
    bool analyze;
    bool targetRectSet;
public:
	VideoCapture cap;
	Rect targetRect;
	int lowThreshold;
	int highThreshold;
    bool freeze;
    Calibration calibration;
    Recorder recorder;

	WebcamOperator(): lowThreshold(100), highThreshold(200), analyze(false), freeze(false), targetRectSet(false), recorder(this)
	{ }

	bool init(int camToOpen = 0)
	{
		//targetRect = Rect(x, y, width, height);
		cap = VideoCapture(camToOpen);
        //recorder.startRecording("out.avi");
		// obsługa błędów !
		return true;
	}
    void setTargetRect(const Rect& rect)
    {
        cout << "Setting target rect with x:" << rect.x << " y: " << rect.y << " width: " << rect.width << " height: " << rect.height << endl;
        targetRect = rect;
        originalFrame(targetRect).copyTo(roleModel);
        targetRectSet = true;
    }
	Mat& makeFrame(bool calibrate = false)
	{
        if(!freeze)
        {
		    cap.read(frame);
            if(targetRectSet)
            {
		        frame(targetRect).copyTo(part);	
            }
        }
		//rectangle(frame, targetRect, Scalar(0, 110, 0));
		if( frame.data )
		{
			if (!freeze && calibrate)
			{
				originalFrame = frame.clone();
				calibration.processImage(frame);
			}
		}
		return frame;
	}

    void setAnalyze(bool v)
    {
        cout << "Setting analyze to: " << v << endl;
        analyze = v;
    }

	Mat& getFrame()
	{
		return frame;
	}
    Mat& getEyeImg()
    {
        return part;    
    }

    Mat& getRoleModel()
    {
        return roleModel;
    }

	void flow(Mat& img)
	{
		Mat hsv;
		cvtColor(img, hsv, COLOR_RGB2HSV);
		vector<Mat> channels;
		split(hsv, channels);
		Mat detectedEdges;
		blur(channels[0], detectedEdges, Size(3, 3));
		
		Canny(detectedEdges, detectedEdges, lowThreshold, highThreshold, 3);

	//	Mat dst = Scalar::all(0);
		Scalar suma = sum(detectedEdges);
		int val = suma[0];
		int eyeThreshold = 3000;
		//cout << (val > eyeThreshold ? "OPEN" : "CLOSE") << "  -> " <<  suma << endl;
		eyeState = val > eyeThreshold;
	}

	bool getEyeState() const
	{ return eyeState; }
};

#endif
