#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <iostream>
#include <list>
#include <cairo.h>
#include <gtk/gtk.h>

class Object;

using namespace std;

class MainWindow
{
protected:
	list<Object*> objects;
	GtkWidget* widget;
public:
	MainWindow(GtkWidget* w): widget(w)
	{
		gtk_widget_set_size_request(widget, 900, 700);
		gtk_window_set_resizable(GTK_WINDOW(widget), FALSE);
	}

	void clear();
	void addObject(Object* obj);
	void redraw();
		
	void draw(cairo_t* cr);
	list<Object*>* getObjects()
	{
		return &objects;
	}

	
		
};

#endif
