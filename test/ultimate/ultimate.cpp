#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <sstream>
#include <vector>

using namespace cv;
using namespace std;

int lowThreshold = 100;
int highThreshold = 200;


void flow(Mat& img)
{
	Mat hsv;
	cvtColor(img, hsv, COLOR_RGB2HSV);
	vector<Mat> channels;
	split(hsv, channels);
	Mat detectedEdges;
	blur(channels[0], detectedEdges, Size(3, 3));
	
	Canny(detectedEdges, detectedEdges, lowThreshold, highThreshold, 3);

//	Mat dst = Scalar::all(0);
	Scalar suma = sum(detectedEdges);
	int val = suma[0];
	int eyeThreshold = 8000;
	cout << (val > eyeThreshold ? "OPEN" : "CLOSE") << "  -> " <<  suma << endl;
	imshow("edge", detectedEdges);
//	waitKey(0);
}



void saveToFile(Mat& img, int num)
{
	char buf[20];
	sprintf(buf, "eyes/eye%d.png", num);
	imwrite(buf, img);
}


int main(int argc, char* argv[0])
{
	Mat image;
//	image = imread("obrazek.png", 1);
//	namedWindow("Display", CV_WINDOW_AUTOSIZE);
//	imshow("image", image);
//	waitKey(0);
int cam = 0;
if(argc > 1)
	++cam;

VideoCapture cap(cam);

if(!cap.isOpened())
{
	std::cout << "can't open webcam" << std::endl;
	return 0;
}
int dt = 15;
Rect rect;
rect.x = 370;
rect.y = 280;
rect.width = 65;
rect.height = 40;
Mat part;
bool first = true;


int numOfFotos = 100;
int freeFrames = 10;
int acc = freeFrames;

while(1)
{
	cap.read(image);
	image(rect).copyTo(part);
	rectangle(image, rect, Scalar(0, 170, 0));
	imshow("asd", image);
	int result = waitKey(27);
	if(result == 27)
	{
		if(!first) break;
		first = false;
	}
	if(!first)
		flow(part);
	if(result == 65361)
		rect.x -= dt;
	else if(result == 65362)
		rect. y -= dt;
	else if(result == 65363)
		rect.x += dt;
	else if(result == 65364)
		rect.y += dt;
	else if(result == 97)
		rect.width -= dt;
	else if(result == 115) rect.width += dt;
	else if(result == 122) rect.height -= dt;
	else if(result == 120) rect.height += dt;
	else if(result == 111) lowThreshold -= 1;
	else if(result == 112) lowThreshold += 1;
	else if(result == 107) highThreshold -= 1;
	else if(result == 108) highThreshold +=1;
//	if(result >= 0)
//		std::cout << result << std::endl;
}

cout << "low threshold: " << lowThreshold << endl << "high threshold: " << highThreshold << endl;
cout << "rect: " << rect << endl;	
}
