#include "Recorder.h"
#include "WebcamOperator.h"

Recorder::Recorder(WebcamOperator* w) : webcam(w), scale(1.0f), fps(10.0), alive(false), writer(nullptr), maxValue(0), eyeOpen(false)
{
}

bool Recorder::isRecording() const
{ return alive; }

void Recorder::startRecording(const string& file)
{
	webcam->makeFrame();
    Mat img = getFrame(0);
    //int ex = static_cast<int>(webcam->cap.get(CV_CAP_PROP_FOURCC)); 
	int ex = -1;
	//ex = VideoWriter::fourcc('M','P','4','3');
	ex = CV_FOURCC('M','P','4','3');
    cout << "codec: " << ex << endl;
    writer = new VideoWriter(file, ex, fps, img.size(), true);
    (*writer) << img;
    alive = true;
}

void Recorder::stopRecording()
{
	if (!alive) return;
    alive = false;
	writer->release();
}

void Recorder::update(int v)
{
	Mat frame = getFrame(v);
	cv::imshow("recorder", frame);
    if(!alive) return;
    (*writer) << frame;
}

void Recorder::setValue(int v, int t)
{
	vvv = v;
	threshold = t;
	maxValue = v * 12 / 10;
}

Mat Recorder::getFrame(int v)
{
    //return webcam->getFrame();
	Mat img =  webcam->getEyeImg();
	cv::resize(img, img, img.size() * 3);
	int height = v*img.size().height / maxValue;
	cv::rectangle(img, cv::Rect(0, 0, 6, height), CV_RGB(0, 128, 255), 5);
	cv::rectangle(img, cv::Rect(0, threshold*img.size().height / maxValue, 5, 2), CV_RGB(0, 255, 0));
	cv::rectangle(img, cv::Rect(0, img.size().height * vvv / maxValue, 5, 2), CV_RGB(255, 0, 0));
	cv::rectangle(img, cv::Rect(16, 15, 7, 7), eyeOpen ? CV_RGB(255, 255, 255) : CV_RGB(0, 0, 0), 2);
	return img;
}

void Recorder::notify(bool eo)
{
	eyeOpen = eo;
}


