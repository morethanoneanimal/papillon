#include "EventTable.h"
#include "Event.h"
#include <iostream>
EventTable::EventTable(): working(false)
{ }

vector<Event*>* EventTable::getEvents()
{
	guard.lock();
	return &events;
}

void EventTable::release()
{
    if(!working)
        clear();
	guard.unlock();
}

void EventTable::clear()
{
	for(Event* e: events)
		delete e;
	events.clear();
}

void EventTable::pushEventSafe(Event* event)
{
	getEvents();
	events.push_back(event);
	release();
}
void EventTable::debug()
{
	getEvents();
	std::cout << "Objects in eventTable: " << events.size() << std::endl;
	for(Event* e: events)
		std::cout << e->toString() << std::endl;
	release();
}
void EventTable::turnOn()
{
    std::cout << "EventTable - turning on" << endl;
}
void EventTable::turnOff()
{
    std::cout << "EventTable - turning off" << endl;
    working = false;
}
