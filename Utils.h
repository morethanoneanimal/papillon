#ifndef UTILS_H
#define UTILS_h

#include <cairo.h>
#include <gtk/gtk.h>

class Utils
{
private:
	Utils()  {}
	Utils(Utils& ref) {}
public:
	static void setNeutralColor(cairo_t* t)
	{
		cairo_set_source_rgb(t, 0.4, 0.4, 1);
	}

	static 	void setSelectedColor(cairo_t* t)
	{
		cairo_set_source_rgb(t, 0.4, 1, 1);
	}

	static void setClickedColor(cairo_t* t)
	{
		cairo_set_source_rgb(t, 0, 1, 0);
	}

	static void setBackgroundColor(cairo_t* t)
	{
		cairo_set_source_rgb(t, 0.3, 0.3, 0.3);
	}
	static void setConfirmedColor(cairo_t* t)
	{
		cairo_set_source_rgb(t, 1, 1, 0);
	}
};

#endif
