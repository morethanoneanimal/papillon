#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <iterator>
#include <vector>
using namespace cv;
using namespace std;

class Calibration
{
protected:
	Rect* rect;
	CascadeClassifier cascade, nestedCascade;
    vector<Rect*> nestedObjectsAbs;
    int targetRectId;
public:
	Calibration(): targetRectId(-1)
	{
		if(! cascade.load("xml/haarcascade_frontalface_alt.xml") )
		{
			std::cout << "Fail when loading cascade file." << std::endl;
		}
		if( ! nestedCascade.load("xml/haarcascade_eye.xml") )
		{
			std::cout << "Fail when loading nested cascade file" << std::endl;
		}
	}

	virtual void processImage(Mat& img)
	{
        if(targetRectId < 0)
    		detectAndDraw(img, cascade, nestedCascade, 1.0, false);
        else
        {
            Mat part;
            img(getTargetRect()).copyTo(part);
            imshow("part", part);
        }
	}

    bool chooseEye(bool left)
    {
        if(nestedObjectsAbs.size() == 0 || nestedObjectsAbs.size() > 2)
        {
            cout << "Inconclusive numbers of eyes: " << nestedObjectsAbs.size() << endl;
            return false;
        }
        if(nestedObjectsAbs.size() == 1)
            return chooseEye(0);
        else
        {
            bool correctOrientation = nestedObjectsAbs[0]->x < nestedObjectsAbs[1]->x;
            if(correctOrientation && left || (!correctOrientation && !left))
                return chooseEye(0);
            else
                return chooseEye(1);
            
        }
    }

    bool chooseEye(int eyeId)
    {
        if(nestedObjectsAbs.size() == 1)
            eyeId = 0;
        targetRectId = eyeId;
    }
    
    Rect& getTargetRect()
    {
        return *nestedObjectsAbs[targetRectId];               
    }

    void clearNestedObjectsAbs()
    {
        for(Rect* r: nestedObjectsAbs)
            delete r;
        nestedObjectsAbs.clear();
    }

	
    void detectAndDraw( Mat& img, CascadeClassifier& cascade, CascadeClassifier& nestedCascade, double scale, bool tryflip )
{
    clearNestedObjectsAbs();
    targetRectId = -1;
    int i = 0;
    double t = 0;
    vector<Rect> faces, faces2;
    const static Scalar colors[] =  { CV_RGB(0,0,255),
        CV_RGB(0,128,255),
        CV_RGB(0,255,255),
        CV_RGB(0,255,0),
        CV_RGB(255,128,0),
        CV_RGB(255,255,0),
        CV_RGB(255,0,0),
        CV_RGB(255,0,255)} ;
    Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );

    cvtColor( img, gray, COLOR_BGR2GRAY );
    resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    equalizeHist( smallImg, smallImg );

    t = (double)cvGetTickCount();
    cascade.detectMultiScale( smallImg, faces,
        1.1, 2, 0
        //|CV_HAAR_FIND_BIGGEST_OBJECT
        //|CV_HAAR_DO_ROUGH_SEARCH
        |0
        ,
        Size(30, 30) );
    if( tryflip )
    {
        flip(smallImg, smallImg, 1);
        cascade.detectMultiScale( smallImg, faces2,
                                 1.1, 2, 0
                                 //|CV_HAAR_FIND_BIGGEST_OBJECT
                                 //|CV_HAAR_DO_ROUGH_SEARCH
                                 |0
                                 ,
                                 Size(30, 30) );
        for( vector<Rect>::const_iterator r = faces2.begin(); r != faces2.end(); r++ )
        {
            faces.push_back(Rect(smallImg.cols - r->x - r->width, r->y, r->width, r->height));
        }
    }
    t = (double)cvGetTickCount() - t;
    //printf( "detection time = %g ms\n", t/((double)cvGetTickFrequency()*1000.) );
    for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
    {
        Mat smallImgROI;
        vector<Rect> nestedObjects;
        Point center;
        Scalar color = colors[i%8];
        int radius;

        double aspect_ratio = (double)r->width/r->height;
        if( 0.75 < aspect_ratio && aspect_ratio < 1.3 )
        {
            center.x = cvRound((r->x + r->width*0.5)*scale);
            center.y = cvRound((r->y + r->height*0.5)*scale);
            radius = cvRound((r->width + r->height)*0.25*scale);
            circle( img, center, radius, color, 3, 8, 0 );
        }
        else
            rectangle( img, cvPoint(cvRound(r->x*scale), cvRound(r->y*scale)),
                       cvPoint(cvRound((r->x + r->width-1)*scale), cvRound((r->y + r->height-1)*scale)),
                       color, 3, 8, 0);
        if( nestedCascade.empty() )
            continue;
        smallImgROI = smallImg(*r);
        nestedCascade.detectMultiScale( smallImgROI, nestedObjects,
            1.1, 2, 0
            //|CV_HAAR_FIND_BIGGEST_OBJECT
            //|CV_HAAR_DO_ROUGH_SEARCH
            //|CV_HAAR_DO_CANNY_PRUNING
            |0
            ,
            Size(30, 30) );
        int nestedObjId = 0;
        for( vector<Rect>::const_iterator nr = nestedObjects.begin(); nr != nestedObjects.end(); nr++ )
        {
            Rect* absRect = new Rect(r->x + nr->x, r->y + nr->y, nr->width, nr->height);
            nestedObjectsAbs.push_back(absRect);
            center.x = cvRound((r->x + nr->x + nr->width*0.5)*scale);
            center.y = cvRound((r->y + nr->y + nr->height*0.5)*scale);
            radius = cvRound((nr->width + nr->height)*0.25*scale);
            circle( img, center, radius, color, 3, 8, 0 );
            center.x -= 30.0f;
            center.y -= 30.0f;
            //putText(img, "oko", center, FONT_HERSHEY_SIMPLEX, 1.0, color, 2);
        }
    }
}
	

	
};

#endif
