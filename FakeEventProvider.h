#ifndef FAKEEVENTPROVIDER_H
#define FAKEEVENTPROVIDER_H

#include "EventProvider.h"
#include <vector>

using namespace std;
class FakeEventProvider: public EventProvider
{
protected:
	pthread_t thread;
	bool generateEvents, eyeHold;
public:
	FakeEventProvider(EventTable& et): EventProvider(et), generateEvents(false), eyeHold(false)
	{}

	virtual bool start()
	{
		pthread_create(&thread, NULL, procedure, (void*)this);	
		return true;
	}

	virtual void shutdown()
	{
		
		
	}
	void eyeOpen()
	{
		std::cout << "FakeEventProvider: eyeOpen()" << std::endl;
		eventTable.pushEventSafe( new Event(EventType::EYE_OPENED, 20) );		
		eyeHold = false;
	}

	void eyeClose()
	{
		if(!eyeHold)
		{
			std::cout << "FakeEventProvider: eyeClose()" << std::endl;
			eventTable.pushEventSafe( new Event(EventType::EYE_CLOSED, 10) );
			eyeHold = true;
		}
	}

	static void* procedure(void* data)
	{
		std::cout << "FakeEventProvider ready to mess !" << std::endl;
		FakeEventProvider* self = (FakeEventProvider*)data;
		while(true) 
		{
			//sleep(6);
			if(self->generateEvents)
			{
				vector<Event*>* events = self->eventTable.getEvents();
				events->push_back(new Event(EventType::EYE_CLOSED, 10));		
				// stuff going on
				self->eventTable.release();	
				std::cout << "FakeEventProvider: messing going on !" << std::endl;
			}
			//sleep(4);
		}
	}

	
};

#endif
