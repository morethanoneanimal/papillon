#ifndef EVENTTABLE_H
#define EVENTTABLE_H

#include <vector>
#include <mutex>

using namespace std;
class Event;

class EventTable
{
protected:
	vector<Event*> events;
	std::mutex guard;
    bool working;

public:
	EventTable();
	vector<Event*>* getEvents();
	void release();
	void clear();
	// te metody sama sie synchronizuja, dla odmiany
	void debug();
	void pushEventSafe(Event* e);
    void turnOn();
    void turnOff();
};

#endif
