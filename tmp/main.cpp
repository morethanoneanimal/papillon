#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <cairo.h>
#include <iostream>
#include "Object.h"
#include "MainWindow.h"
#include "EventTable.h"
#include "HelloContent.h"
#include "FakeEventProvider.h"
#include "WebcamEventProvider.h"
#include "WebcamOperator.h"
#include "HorizontalImgAnalyzer.h"

GtkWidget* window;
GtkWidget* da;
MainWindow* mainWindow;
EventTable eventTable;
EventProvider* eventProvider;
FakeEventProvider* fakeProvider;
WebcamEventProvider* webcamEventProvider;
WebcamOperator webcamOperator;
GdkPixbuf* pix;
bool drawCv = true;
Mat src;


bool CalibrationProcess()
{
    while(true)
    {
        Mat& img = webcamOperator.makeFrame(true);
        imshow("calibration", img);
        int key = waitKey(20);
        if(key > 0)
            cout << "key pressed: " << key << endl;
        if(key == 113) 
        {
            webcamOperator.freeze = !webcamOperator.freeze;
            cout << "Webcam freeze: " << webcamOperator.freeze << endl;
        }
        else if(webcamOperator.freeze && (key == 108 || key == 112 ) )
        {
            if(webcamOperator.calibration.chooseEye( key == 108 ? 0 : 1 ))
            {
                webcamOperator.setTargetRect( webcamOperator.calibration.getTargetRect() ); // nie jest to najszczesliwszy kawalke kodu ...
                cout << "eye choosen !" << endl;
                webcamOperator.freeze = false;
                break;
            }
        }
    }
    // tmp
    HorizontalImgAnalyzer *h = new HorizontalImgAnalyzer();
    webcamEventProvider->addAnalyzer(h);
    WebcamEventProvider::procedure(webcamEventProvider);
    eventTable.turnOn();
    int idx = 0;
    while(true)
    {
        //webcamOperator.makeFrame(false);
        //int w = h->processImg(webcamOperator.getEyeImg() );
        //cout << idx++ << " " << w << endl;
        //cout << "about to show eye" << endl;
        //imshow("eye", webcamOperator.getEyeImg() );
        //int key = waitKey(20);
    }

}

static bool expose_ev(GtkWidget* widget)
{
//	drawCv = false;
    if(drawCv)
    {
        //IplImage* opencvImage = cvCloneImage(&(IplImage)src);
        //pix = gdk_pixbuf_new_from_data((guchar*) opencvImage->imageData,
         //   GDK_COLORSPACE_RGB, FALSE, opencvImage->depth, opencvImage->width,
         //   opencvImage->height, (opencvImage->widthStep), NULL, NULL);

        //gdk_draw_pixbuf(widget->window,
      //widget->style->fg_gc[GTK_WIDGET_STATE (widget)], pix, 0, 0, 0, 0,
      //opencvImage->width, opencvImage->height, GDK_RGB_DITHER_NONE, 0, 0); /* Other possible values are  GDK_RGB_DITHER_MAX,  GDK_RGB_DITHER_NORMAL */
        return FALSE;
    }
    else
    {
	cairo_t* cr = gdk_cairo_create(widget->window);
	cairo_set_font_size(cr, 40);
	mainWindow->draw(cr);
	}
	return FALSE;
}

static bool on_key_pressed(GtkWidget* widget, GdkEventKey* data)
{
	if(data->keyval == GDK_s)
	{
		fakeProvider->eyeClose();
	}
	return FALSE;
}

static bool on_key_up(GtkWidget* widget, GdkEventKey* data)
{
	if(data->keyval == GDK_s)
	{
		fakeProvider->eyeOpen();
	}
	return FALSE;
}

int main(int argc, char* argv[])
{
	gtk_init(&argc, &argv);
	window = gtk_window_new( GTK_WINDOW_TOPLEVEL );
	mainWindow = new MainWindow(window);
	da = gtk_drawing_area_new();
	gtk_container_add( GTK_CONTAINER( window ), da);

	g_signal_connect( G_OBJECT(da), "expose-event", G_CALLBACK(expose_ev), NULL);
	g_signal_connect( G_OBJECT(window), "key_press_event", G_CALLBACK(on_key_pressed), NULL );
	g_signal_connect( G_OBJECT(window), "key_release_event", G_CALLBACK(on_key_up), NULL );
	
    webcamOperator.init();
	eventProvider = fakeProvider = new FakeEventProvider(eventTable);
	eventProvider = webcamEventProvider = new WebcamEventProvider(eventTable, &webcamOperator);
    CalibrationProcess();
    //webcamEventProvider->init(); // nowy watek !
	
	HelloContent hello(mainWindow, nullptr, eventTable);
	hello.init();
	gtk_widget_show_all(window);
	gtk_main();
	return 0;
}
