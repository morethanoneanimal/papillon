#include "CamEventProvider.h"
#include <iostream>

CamEventProvider::CamEventProvider(EventTable& et): EventProvider(et), shouldExit(false)
{
}

bool CamEventProvider::init()
{
	std::cout << "CamEventProvider init()" << std::endl;

	return true;
}

void CamEventProvider::shutdown()
{
	std::cout << "CamEventProvider shutdown()" << std::endl;
}
