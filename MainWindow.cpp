#include "MainWindow.h"
#include "Object.h"

void MainWindow::clear()
{
	objects.clear();
	redraw();
}

void MainWindow::addObject(Object* obj)
{
	objects.push_back(obj);
}

void MainWindow::redraw()
{
	gtk_widget_queue_draw(widget);
}

void MainWindow::draw(cairo_t* cr)
{
	cairo_set_source_rgb(cr, 0.2, 0.2, 0.2);
	cairo_paint(cr);
	//std::cout << "obj count: " << objects.size() << std::endl;
	for(Object* o: objects)
		o->draw(cr);
}
