#ifndef WEBCAMEVENTPROVIDER_H
#define WEBCAMEVENTPROVIDER_H

#include "EventProvider.h"
#include "WebcamOperator.h"
#include <vector>
#include "ImgAnalyzer.h"

class WebcamEventProvider: public EventProvider
{
protected:
	pthread_t thread;

	bool eyeOpenState;
	int acc;
	int framesRequired;
	EventType currentType;
    vector<ImgAnalyzer*> analyzers;
public:
	WebcamOperator* webcam;
	WebcamEventProvider(EventTable& et, WebcamOperator* w): EventProvider(et), acc(0), eyeOpenState(false), framesRequired(1), webcam(w)
	{ }

    void clearAnalyzers()
    {
        for(ImgAnalyzer* a: analyzers) 
            delete a;
        analyzers.clear();
    }
    void addAnalyzer(ImgAnalyzer* analyzer)
    {
        analyzer->init(webcam);
        analyzers.push_back(analyzer);
    }

	virtual bool start()
	{
		pthread_create(&thread, NULL, procedure, (void*)this);
		return true;
	}	

	virtual void shutdown()
	{
	}

	void eyeOpened()
	{
		if(currentType == EventType::EYE_OPENED)
		{
			acc = 0;
			return;
		}
		if(++acc == framesRequired)
		{
			// idzie event !
			acc = 0;
			vector<Event*>* events = eventTable.getEvents();
			events->push_back(new Event(EventType::EYE_OPENED, 10) );
			currentType = EventType::EYE_OPENED;
			eventTable.release();	
			std::cout << "EYE OPENED !!!!!!!!!!!!" << std::endl;
			webcam->recorder.notify(true);
		}

	}

	void eyeClosed()
	{
		if(currentType == EventType::EYE_CLOSED)
		{
			acc = 0;
			return;
		}
		if(++acc == framesRequired)
		{
			acc = 0;
			vector<Event*>* events = eventTable.getEvents();
			events->push_back(new Event(EventType::EYE_CLOSED, 10));
			currentType = EventType::EYE_CLOSED;
			eventTable.release();
			std::cout << "EYE CLOSED !!!!!!!!!!" << std::endl;
            playVoice(false);
			webcam->recorder.notify(false);
		}
	}
	
	//Mat& getImage()
	//{
	//	return webcam.getFrame();
	//}


	static void* procedure(void* data)
	{
		WebcamEventProvider* self = (WebcamEventProvider*)data;
		std::cout << "WebcamEventProvider procedure()" << std::endl;
		self->webcam->recorder.startRecording("out.avi");
		while(true)
		{
			Mat& img = self->webcam->makeFrame();
            if(self->analyzers.size() > 0)
            {
                if( self->analyzers[0]->analyze( self->webcam->getEyeImg() ) )
                    self->eyeOpened();
                else
                    self->eyeClosed();
            }
			self->webcam->recorder.update(self->analyzers[0]->getLastValue());
            imshow("webcameventprovider", img); 
			if (cv::waitKey(20) != -1)
				break;
		}
		self->webcam->recorder.stopRecording();
		self->analyzers[0]->printDiagnosticData();
		return nullptr;
	}

    void playVoice(bool open)
    {
		return;
        if(open)
            system("mplayer ./sounds/eyeopen.mp3 -really-quiet &");
        else
            system("mplayer ./sounds/eyeclosed.mp3 -really-quiet &");
    }
	

};


#endif
