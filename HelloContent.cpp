#include "HelloContent.h"
#include "Object.h"
#include "MainWindow.h"
#include "Event.h"
#include <sys/types.h>
#include <pthread.h>
#include <vector>
#include "MorseContent.h"

using namespace std;


void HelloContent::init()
{
	std::cout << "HelloContent init() " << std::endl;
	mainWindow->clear();
	for(Object* o: objects)
		mainWindow->addObject(o);
	pthread_t t;
	pthread_create( &t, NULL, childProcess, (void*)this); 

}

void HelloContent::routeSelection(int sel)
{
	if(sel == 1)
	{
		morseContent->init();
	}
	else
		std::cout << "Hello content - uknown selection " << sel << std::endl;
}

void* HelloContent::childProcess(void* data)
{
	HelloContent* self = (HelloContent*)data;
	int sleepTime = 500 * 1000, tick = 5, clickedTick = 5;
	int acc = 0;
	int curSel = 0;
	self->objects[curSel]->select();
	std::cout << "awesome !" << std::endl;
	bool clicked = false;
	while(1)
	{
		++acc;
		vector<Event*>* events = self->eventTable.getEvents();
		for(Event* e: *events)
		{
			if(e->eventType == EventType::EYE_CLOSED)
			{
				std::cout << "HelloContent: eye closed, something has to be done ." << curSel << std::endl;
				acc = 0;
				//clicked = true;
				self->objects[curSel]->clicked();
				self->mainWindow->redraw();
			}
		}
		self->eventTable.clear();
		self->eventTable.release();
		if(!clicked && acc == tick)
		{
			self->objects[curSel]->deselect();
			curSel = ++curSel % self->objects.size();
			self->objects[curSel]->select();
			acc = 0;
			self->mainWindow->redraw();
		}
		if(clicked && acc == clickedTick)
		{
			std::cout << "HelloContent: tu powinien byc odpowiedni init" << std::endl;
			self->routeSelection(curSel);
			return nullptr;
		}
		self->mainWindow->redraw();
		usleep(sleepTime);
	}
}
