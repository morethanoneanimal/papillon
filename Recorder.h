#ifndef RECORDER_H
#define RECORDER_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

class WebcamOperator;

class Recorder
{
protected:
    VideoWriter* writer;
    bool alive;
    WebcamOperator* webcam;
	int vvv, threshold;
	bool eyeOpen;
public:
    float scale;
    double fps;

	int maxValue;
    
    Recorder(WebcamOperator* w);

    bool isRecording() const;
    void startRecording(const string& file);
    void stopRecording();
    void update(int v = 20);
    Mat getFrame(int v);

	void setValue(int v, int t);   
	void notify(bool eo);

};

#endif
