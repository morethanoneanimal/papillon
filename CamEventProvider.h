#ifndef CAMEVENTPROVIDER_H
#define CAMEVENTPROVIDER_H

#include "EventProvider.h"

class CamEventProvider: public EventProvider
{
protected:
	bool shouldExit;
public:
	CamEventProvider(EventTable& et);

	virtual bool init();
	virtual void shutdown();
};

#endif
