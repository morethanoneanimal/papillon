# System do wspomagania osób dotkniętych paraliżem #
Dla osób dotkniętych paraliżem wskutek wypadku lub poważnego schorzenia ednym z problemów jest komunikacja i interakcja z otoczeniem. W skrajnych przypadkach takie osoby są w stanie kontrolować tylko jedno oko. W celu ułatwienia im komunikacji ze światem, a zatem w celu polepszenia jakości ich życia należy skonstruować system ułatwiający im wyrażanie myśli oraz wydawanie prostych komend.

Ten projekt jest propozycją implementacji takiego systemu.

Nazwa robocza "Papillon" wzięła się z nazwy filmu "Le Scaphandre et le papillon" opowiadającego historię sparaliżowanego człowieka, który pisze książkę.
### Wykorzystywane technologie ###

* OpenCV - algorytmy związanie z rozpoznawaniem twarzy (haar cascade) i przetwarzanie obrazu
* GTK - interfejs użytkownika


### Struktura kodu ###

Program składa się z dwóch głównych części: jednej odpowiedzialnej za wyświetlanie GUI i obsługę zdarzeń oraz drugiej, odpowiedzialnej za generowanie tych zdarzeń.

Zdarzenia przechowywane są w klasie EventTable, która zapewnia równoległy dostęp z różnych wątków.

Za generowanie zdarzeń odpowiadają klasy implementujące interfejs EventProvider. W tym momencie dostępne są dwie klasy implementujące ten interfejs:

 * WebcamEventProvider
 * FakeEventProvider (słuzy do mockowania)


### Dokumenty ###
http://student.agh.edu.pl/~danek/TOIK/Wizja.pdf