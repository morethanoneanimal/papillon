#ifndef HORIZONTAL_IMG_ANALYZER_H
#define HORIZONTAL_IMG_ANALYZER_H
#include <cstdlib>
#include <iostream>
#include <vector>
#include <time.h>
#include <fstream>
#include "ImgAnalyzer.h"

using namespace cv;
using namespace std;

class HorizontalImgAnalyzer: public ImgAnalyzer
{
protected:
	int openEyeWar, threshold, lastValue;
	int executions;
	double time;
	int calc(Mat& img, int row)
	{
		int total = 0;
		Vec3b prev = img.at<Vec3b>(row, 0);
		for(int i = 1; i < img.cols; ++i)
		{
			Vec3b current = img.at<Vec3b>(row, i);
			int val = abs(current[0] - prev[0]) + abs(current[1] - prev[1]) + abs(current[2] - prev[2]);
			total += val;
			prev = current;
		}
		return total;
	}
	
	float odchylenie(vector<int>& results, float avg)
	{
		float total = 0.0f;
		for(int i: results)
		{
			total += pow((avg - i), 2);
		}
		total /= pow(results.size(), 1);
		return total;
	}

	void calcThreshold()
	{
		std::ifstream file("hia.dat");
		if (file.is_open())
		{
			int a, b;
			file >> a >> b;
			threshold = a * openEyeWar / b;
			std::cout << "HorizontalImageAnalyzer, threshold: " << threshold << endl;
		}
		else
		{
			std::cout << "HorizontalImageAnalyzer: no file found, default values applied" << std::endl;
			threshold = openEyeWar / 2;
		}
		
	}

public:
	HorizontalImgAnalyzer() : executions(0), time(0.0)
	{}
    virtual void init(WebcamOperator* w)
    {
        ImgAnalyzer::init(w);
        cout << "kalibracja !" << endl;
        openEyeWar = processImg(webcamOperator->getRoleModel());
		imshow("ROLE MODEL", webcamOperator->getRoleModel());
		waitKey();
		calcThreshold();
        cout << "wariancja: " << openEyeWar << endl << "threshold: " << threshold <<  endl;        
    }
   
	virtual bool analyze(Mat& img)
	{
        return processImg(img) > threshold;
    }
	virtual int getValue() const
	{
		return openEyeWar;
	}
	virtual int getLastValue() const
	{
		return lastValue;
	}
	virtual int getThreshold() const
	{
		return threshold; 
	}
    int processImg(Mat& img)
    {
		clock_t startTime = clock();
		vector<int> results;
		int max = 0, total = 0;
		float avg = 0.0f;
		for(int i = 0; i < img.rows; ++i)
		{
			int v = calc(img, i);
			results.push_back(v);
			total += v;
			max = v > max ? v : max;
		}
		avg = total / (double)img.rows;
		float war = odchylenie(results, avg);
		int maxDiff = max - avg;
		//cout << "avg: " << avg << endl;
		//cout << "maxDiff: " << maxDiff << "; max: " << max << endl;
		//cout << "wariancja: " << war << endl;
		factor = 1.0f;
        //cout << war << endl;
		lastValue = war;
		clock_t endTime = clock();
		time += (endTime - startTime) / (double)CLOCKS_PER_SEC;
		++executions;
        return war;
	}

	virtual void printDiagnosticData() const
	{
		std::cout << "HorizontalImgAnalyzer avg time: " << time * 1000 / (double)executions << endl;


	}
};

#endif
