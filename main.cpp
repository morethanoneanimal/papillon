#include <iostream>
#include "EventTable.h"
#include "WebcamEventProvider.h"
#include "WebcamOperator.h"
#include "HorizontalImgAnalyzer.h"

EventTable eventTable;
EventProvider* eventProvider;
WebcamEventProvider* webcamEventProvider;
WebcamOperator webcamOperator;
Mat src;


bool CalibrationProcess()
{
    while(true)
    {
        Mat& img = webcamOperator.makeFrame(true);
		if (img.data)
			imshow("calibration", img);
        int key = waitKey(20);
        if(key > 0)
            cout << "key pressed: " << key << endl;
        if(key == 113) 
        {
            webcamOperator.freeze = !webcamOperator.freeze;
            cout << "Webcam freeze: " << webcamOperator.freeze << endl;
        }
        else if(webcamOperator.freeze && (key == 108 || key == 112 ) )
        {
            if(webcamOperator.calibration.chooseEye( key == 108  ))
            {
                webcamOperator.setTargetRect( webcamOperator.calibration.getTargetRect() ); // nie jest to najszczesliwszy kawalke kodu ...				
                cout << "eye choosen !" << endl;
                webcamOperator.freeze = false;
                break;
            }
        }
    }
    // tmp
    HorizontalImgAnalyzer *h = new HorizontalImgAnalyzer();
    webcamEventProvider->addAnalyzer(h);
	webcamOperator.recorder.setValue(h->getValue(), h->getThreshold());
    WebcamEventProvider::procedure(webcamEventProvider);
    eventTable.turnOn();  
	return true;
}


int main(int argc, char* argv[])
{
    webcamOperator.init();
	eventProvider = webcamEventProvider = new WebcamEventProvider(eventTable, &webcamOperator);
    CalibrationProcess();
    //webcamEventProvider->init(); // nowy watek !
	return 0;
}
