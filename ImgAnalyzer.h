#ifndef IMGANALYZER_H
#define IMGANALYZER_H

#include <opencv2/opencv.hpp>
#include "WebcamOperator.h"
using namespace cv;
class ImgAnalyzer
{
protected:
	float factor;
    WebcamOperator* webcamOperator;
public:
	ImgAnalyzer(): factor(0.0f), webcamOperator(nullptr)
	{ }
    virtual void init(WebcamOperator* w)
    {
        webcamOperator = w;
    }
	virtual bool analyze(Mat& img) = 0;
	virtual float getFactor() const
	{ return factor; }
	virtual int getLastValue() const = 0;
	virtual void printDiagnosticData() const = 0;
};
#endif
